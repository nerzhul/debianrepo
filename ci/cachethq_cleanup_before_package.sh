#! /bin/bash
set -x
find . -type d -name ".git" -exec rm -Rf {} \;
find . -type d -name ".svn" -exec rm -Rf {} \;