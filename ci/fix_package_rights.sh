#!/usr/bin/env bash
if [ -z ${1} ]; then
    echo "Missing ${0} argument"
    exit 1
fi

find "${1}" -type d -exec chmod 755 {} \;
find "${1}" -type f -not -name postinst -exec chmod 644 {} \;
find "${1}" -type f -name postinst -exec chmod 755 {} \;
find "${1}" -type f -name preinst -exec chmod 755 {} \;