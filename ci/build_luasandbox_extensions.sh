#!/usr/bin/env bash

for _module in cjson geoip lfs postgres rjson sax snappy socket ssl struct systemd; do
		sed "/UPDATE_COMMAND/a\\
INSTALL_COMMAND \"\"" -i lua_sandbox_extensions-${LUASANDBOX_EXTENSIONS_TECH_VERSION}/${_module}/CMakeLists.txt
done

cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=../luasandbox-extensions/usr \
    -DCMAKE_INSTALL_LIBDIR=../../luasandbox-extensions/usr/lib \
    -DCMAKE_INSTALL_DATAROOTDIR=../../luasandbox-extensions/usr/lib \
    -DEXT_circular_buffer=1 \
    -DEXT_cjson=1 \
    -DEXT_compat=1 \
    -DEXT_elasticsearch=1 \
    -DEXT_heka=1 \
    -DEXT_geoip=1 \
    -DEXT_lfs=1 \
    -DEXT_lpeg=1 \
    -DEXT_lsb=1 \
    -DEXT_rjson=1 \
    -DEXT_sax=1 \
    -DEXT_syslog=1 \
    -DEXT_socket=1 \
    -DEXT_ssl=1 \
    -DEXT_struct=1 \
    -DEXT_systemd=1 \
    -DEXT_zlib=1 \
    lua_sandbox_extensions-${LUASANDBOX_EXTENSIONS_TECH_VERSION}

make install
if [ $? -ne 0 ]; then
    echo "Failed to perform the main make install"
    exit 1
fi

for _module in cjson lfs sax socket ssl struct systemd; do
        echo "Building module ${_module}"
		cd ${_module}/ep_${_module}-prefix/src/ep_${_module}-build && make install
		if [ $? -ne 0 ]; then
		    echo "Failed to build ${_module} module"
            exit 1
        fi
		cd ../../../../
done
