# My repository

This is my Debian resository for custom built packages

## How to use it

Download & install the key from here

```
https://nerzhul.gitlab.io/debianrepo/repo.key
```

Add the following line to your sources.list

```
deb https://nerzhul.gitlab.io/debianrepo stretch main
```

## Available packages

* cachethq
* etcd
* flanneld
* hindsight
* kube-apiserver
* kube-controller-manager
* kube-proxy
* kube-scheduler
* kubectl
* kubelet
* libluasandbox (+ extensions)
* minio (server & client)
* rundeck
* sonatype-nexus
* vault (hashicorp)